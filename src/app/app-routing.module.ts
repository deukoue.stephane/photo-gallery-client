import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    children: [
      { path: '', redirectTo: 'category', pathMatch: 'full' },
      {
        path: "category",
        loadChildren: () => import("./main/main.module").then(m => m.MainModule),
      },
      {
        // fallback URL when nothing else matches
        path: "**",
        loadChildren: () => import("./static/not-found.module").then(m => m.NotFoundModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
