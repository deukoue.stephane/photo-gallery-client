import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {Category, CategoryRequestBody} from "../dto/dto";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient, HttpEvent} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";

/**
 * @author Stephane Deukoue
 */
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private readonly categories: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>([]);
  readonly categories$ = this.categories.asObservable();

  constructor(private http: HttpClient, private toastr: ToastrService) {
  }

  /** Create a new category on the server
   * @param category an object containing the name of the new category since it's the only property we need
   * @return Observable<boolean> true or false if the category was created or not
   */
  createNewCategory(category: CategoryRequestBody): Observable<boolean> {
    return this.http.post<boolean>(`${environment.apiUrl}/categories/add`, category);
  }

  /** Get the list of all categories present on the server and store in an observer variable for later use
   */
  getAllCategories(): void {
    this.http.get<Category[]>(`${environment.apiUrl}/categories/`).subscribe(
      categories => this.categories.next(categories),
      () => this.toastr.error("An error occurred while getting the list of all categories")
    )
  }

  /** Update a category on the server
   * @param categoryId the id of the category that's going to be updated
   * @param category an object containing the name of the category
   * @return Observable<boolean> true or false if the category was updated or not
   */
  updateCategory(categoryId: string, category: CategoryRequestBody): Observable<Category> {
    return this.http.post<Category>(`${environment.apiUrl}/categories/update/${encodeURIComponent(String(categoryId))}`, category);
  }

  /** Delete the pictures of a category from the server
   * @param categoryId the id of the category itself
   * @param pictureId the id of the picture to be deleted
   * @return Observable<boolean> true or false if the picture was deleted or not
   */
  deleteCategoryPicture(categoryId: string, pictureId: string): Observable<boolean> {
    return this.http.delete<boolean>(
      `${environment.apiUrl}/categories/delete/picture/${encodeURIComponent(String(categoryId))}/${encodeURIComponent(String(pictureId))}`)
  }

  /** Delete a category from the server
   * @param categoryId the id of the category that's going to be deleted
   * @return Observable<boolean> true or false if the category was deleted or not
   */
  deleteCategory(categoryId: string): Observable<boolean> {
    return this.http.delete<boolean>(`${environment.apiUrl}/categories/delete/${encodeURIComponent(String(categoryId))}`)
  }

  /** Upload a new picture
   * @param categoryId the id of the category
   * @param files list of all images
   * @return Observable<boolean> true or false if the pictures were successfully saved or not
   */
  uploadPicture(categoryId: string, files: File[]): Observable<HttpEvent<boolean>> {
    const fd = new FormData();
    fd.append("categoryId", categoryId);
    files.forEach(file => fd.append("files", file));

    return this.http.post<boolean>(
      `${environment.apiUrl}/categories/upload`, fd,
      {
        responseType: "text" as any,
        observe: "events",
        reportProgress: true
      }
    )
  }
}
