import {Injectable} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";

/**
 * @author Deukoue Stephane
 */
@Injectable({
    providedIn: "root"
})
export class SharedService {
    /** Creates url from the blob file so that we can use it later in the image tag as src
     * @param blob the blob file
     * @param sanitizer used to purify the url in order to bypass the security constraints
     * @param defaultList list containing the urls of the blob files
     *
     * @return any[] defaultList or List
     */
    createImagesFromBlob(blob: Blob, sanitizer: DomSanitizer, defaultList: any[]): any[] {
        const urlCreator = window.URL || window.webkitURL;
        const imageUrl = urlCreator.createObjectURL(blob);
        defaultList.push(sanitizer.bypassSecurityTrustUrl(imageUrl));

        return defaultList;
    }
}
