import {Injectable} from "@angular/core";
import {Title} from "@angular/platform-browser";

/**
 * @author Stephane Deukoue
 */
@Injectable({providedIn: "root"})
export class TitleService {
    /**
     * Remember the current Title. Can be an ngx-translate key if isTranslated is true
     */
    public currentTitle: string = "";

    constructor(private title: Title) {}

    /**
     * Set new page title
     * @param newTitle the new page title
     */
    public setTitle(newTitle: string): void {
        this.currentTitle = newTitle;
        this.title.setTitle(`${newTitle} - cosee`);
    }
}
