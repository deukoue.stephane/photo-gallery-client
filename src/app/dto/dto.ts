import {SafeUrl} from "@angular/platform-browser";

export interface Category {
  categoryId: string;
  name: string;
  color: string;
  createAt: number;
  pictures: Picture[];
}

export interface Picture {
  pictureId: string;
  categoryId?: string;
  file: SafeUrl;
  createAt: number;
}

export interface CategoryRequestBody {
  name: string;
}

export interface UploadStatus {
  status: string;
  percent: number;
}
