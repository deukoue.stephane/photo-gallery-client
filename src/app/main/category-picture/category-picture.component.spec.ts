import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryPictureComponent } from './category-picture.component';

describe('CategoryPictureComponent', () => {
  let component: CategoryPictureComponent;
  let fixture: ComponentFixture<CategoryPictureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryPictureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
