import {Component, OnDestroy, OnInit} from '@angular/core';
import {CategoryService} from "../../core/category.service";
import {DomSanitizer} from "@angular/platform-browser";
import {BehaviorSubject, Subscription} from "rxjs";
import {Category, CategoryRequestBody, Picture} from "../../dto/dto";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-category-picture',
  templateUrl: './category-picture.component.html',
  styleUrls: ['./category-picture.component.scss']
})
export class CategoryPictureComponent implements OnInit, OnDestroy {

  categories: Category[] = [];
  listOfPictures: Picture[] = [];
  currentPictures: Picture[] = [];

  private readonly currentCategoryId: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  currentCategory$ = this.currentCategoryId.asObservable();

  currentCategory: string;
  sub$: Subscription;
  category: CategoryRequestBody = {name: ""};
  isEditing: boolean = false;

  constructor(
    private categoryService: CategoryService,
    private readonly activeRoute: ActivatedRoute,
    private readonly sanitizer: DomSanitizer,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.categoryService.getAllCategories();
  }

  ngOnDestroy(): void {
    this.sub$.unsubscribe();
  }

  ngOnInit(): void {
    this.initCategoriesAndPictures();
    const categoryId = this.activeRoute.snapshot.paramMap.get("cId") || this.activeRoute.snapshot.queryParams.cId;
    if (categoryId) {
      this.currentCategoryId.next(categoryId);
    }
  }

  /** Add a new category on the server
   */
  addCategory(): void {
    this.categoryService.createNewCategory(this.category).subscribe(
      created => {
        if (created) {
          this.close();
        }
      },
      () => this.toastr.error("The category could not be created because the category already exists"));
  }

  /** Update a category on the server
   */
  updateCategory(): void {
    this.categoryService.updateCategory(this.currentCategoryId.value, this.category).subscribe(
      updated => {
        if (updated) {
          this.close();
        }
      },
      () => this.toastr.error("The category could not be updated")
    );
  }

  /** Delete a category from the server
   * @param categoryId the id of the category
   */
  deleteCategory(categoryId: string): void {
    this.categoryService.deleteCategory(categoryId).subscribe(
      deleted => {
        if (deleted) {
          this.currentCategoryId.next(null);
          this.categoryService.getAllCategories();
        }
      },
      () => this.toastr.error("The category could not be deleted")
    );
  }

  /** Delete pictures of a category from the server
   * @param pictures holds the list of pictures to be deleted
   */
  deletePicture(pictures: Picture[]): void {
    pictures.forEach(picture => {
      this.categoryService.deleteCategoryPicture(picture.categoryId, picture.pictureId).subscribe(
        deleted => {
          if (deleted) {
            this.categoryService.getAllCategories();
          }
        },
        () => this.toastr.error("The selected pictures could not be deleted")
      );
    });
  }

  /** Get list of pictures of a category
   * @param categoryId the id of the category
   */
  getCategoryPictures(categoryId: string): void {
    this.currentCategoryId.next(categoryId);
    this.location.go("/category", `cId=${categoryId}`);
    this.currentPictures = this.listOfPictures.filter(p => p.categoryId === categoryId);
  }

  /** Init the list of all categories and pictures to be displayed f
   */
  private initCategoriesAndPictures(): void {
    this.sub$ = this.categoryService.categories$.subscribe(categories => {
      let pictures: Picture[] = [];
      this.categories = categories

      categories.forEach(item => {
        pictures.push(...this.adjustPictureUrl(item.pictures, item.categoryId));
      });

      this.listOfPictures = pictures;
      if (!this.currentCategoryId.value) {
        this.currentCategoryId.next(categories[0]?.categoryId);
      }
      this.currentPictures = this.listOfPictures.filter(p => p.categoryId === this.currentCategoryId?.value);
    });
  }

  /** Format the url of the picture so that we can display it in the dom
   * @param pictures holds the list of pictures
   * @param categoryId the id of the category
   */
  private adjustPictureUrl(pictures: Picture[], categoryId: string): Picture[] {
    let images: Picture[] = [];
    pictures.forEach(picture => {
      picture.file = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + picture.file);
      picture.categoryId = categoryId;
      images.push(picture);
    });

    return images;
  }

  /** Close the add category modal
   */
  private close(): void {
    this.category = {name: ""};
    this.categoryService.getAllCategories();
    document.getElementById("close").click();
  }
}
