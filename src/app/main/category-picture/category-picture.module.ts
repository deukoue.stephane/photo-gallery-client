import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {CategoryPictureComponent} from "./category-picture.component";
import {ImageSelectorModule} from "../../shared/components/image-seletor/image-selector.module";
import {FormsModule} from "@angular/forms";

/**
 * @author Stephane Deukoue
 */
@NgModule({
  declarations: [
    CategoryPictureComponent,
  ],
	imports: [
		CommonModule,
		RouterModule.forChild([{
			path: "",
			component: CategoryPictureComponent
		}]),
		ImageSelectorModule,
		FormsModule
	]
})
export class CategoryPictureModule {
}
