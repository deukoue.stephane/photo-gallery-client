import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";


/**
 * @author Stephane Deukoue
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: "",
        loadChildren: () => import("./category-picture/category-picture.module").then(m => m.CategoryPictureModule),
      },
      {
        path: "upload",
        loadChildren: () => import("./upload-user-picture/upload-user-picture.module").then(m => m.UploadUserPictureModule),
      }
    ])
  ]
})
export class MainModule {
}
