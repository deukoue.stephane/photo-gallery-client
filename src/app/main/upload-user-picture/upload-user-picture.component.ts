import {Component, OnInit} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";
import {CategoryService} from "../../core/category.service";
import {UploadStatus} from "../../dto/dto";
import {HttpEvent, HttpEventType} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedService} from "../../core/shared.service";
import {ToastrService} from 'ngx-toastr';

/**
 * @author Stephane Deukoue
 */
@Component({
  selector: "app-upload-user-picture",
  templateUrl: "./upload-user-picture.component.html",
  styleUrls: ["./upload-user-picture.component.scss"]
})
export class UploadUserPictureComponent implements OnInit {
  files: File[] = [];
  images: any[] = [];
  categoryId: string;
  progress: UploadStatus = {status: "", percent: 0};

  constructor(
    private readonly sharedService: SharedService,
    private categoryService: CategoryService,
    private sanitizer: DomSanitizer,
    private readonly router: Router,
    private readonly activeRoute: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.categoryId = this.activeRoute.snapshot.paramMap.get("cId") || this.activeRoute.snapshot.queryParams.cId;
  }

  onDragOverFile(event: any): void {
    event.stopPropagation();
    event.preventDefault();
  }

  async onDropFile(event: DragEvent): Promise<void> {
    event.preventDefault();
    if (event.dataTransfer.files.length > 0) {
      await this.storeSelectedFiles(event.dataTransfer.files);
    }
  }

  async selectFile(event: any): Promise<void> {
    if (event.target.files.length > 0) {
      await this.storeSelectedFiles(event.target.files);
    }
  }

  /** Removes a file from the list of files that will be uploaded to the server
   * @param index the index of the file in the list
   */
  deletePicture(index: number): void {
    if (index === 0) {
      this.files.shift();
      this.images.shift();
    } else {
      this.files.splice(index, index);
      this.images.splice(index, index);
    }
  }

  upload(): void {
    this.categoryService.uploadPicture(this.categoryId, this.files).subscribe(
      done => {
        this.progressLoop(done);
        if (this.progress.percent >= 100) {
          this.categoryService.getAllCategories();
          this.router.navigate(["../category", {cId: this.categoryId}]).then(
            () => this.toastr.success("Your pictures were successfully uploaded"));
        }
      },
      () => this.toastr.error("Your pictures could not be uploaded"));
  }

  private alreadyExist(file: File): boolean {
    return this.files.map(f => f.name).includes(file.name);
  }

  /** Stores the files the user has uploaded to the list of file to be stored on the server and created blob files
   * in order to preview the pictures before saving on the server
   * @param files holds the list of pictures uploaded
   */
  private async storeSelectedFiles(files: FileList): Promise<void> {
    const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
    for (let i = 0; i < files.length; i++) {

      if (validImageTypes.includes(files[i].type) && !this.alreadyExist(files[i])) {
        this.files.push(files[i]);
      }
    }
    this.images = [];
    for (const file of this.files) {
      const blob = new Blob([new Uint8Array(await file.arrayBuffer())], {type: file.type});
      this.images = this.sharedService.createImagesFromBlob(blob, this.sanitizer, this.images);
    }
  }

  private updateStatus(loaded: number, total: number | undefined, status: string): void {
    this.progress = {status: status, percent: Math.round(100 * loaded / total)}
  }

  private progressLoop(event: HttpEvent<boolean>): void {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        this.updateStatus(event.loaded, event.total, "UPLOADING");
        break;
    }
  }
}
