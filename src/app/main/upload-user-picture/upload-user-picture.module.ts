import {NgModule} from "@angular/core";
import {NgbProgressbarModule} from "@ng-bootstrap/ng-bootstrap";
import {CommonModule} from "@angular/common";
import {UploadUserPictureComponent} from "./upload-user-picture.component";
import {RouterModule} from "@angular/router";

/**
 * @author Stephane Deukoue
 */
@NgModule({
    imports: [
        CommonModule,
        NgbProgressbarModule,
      RouterModule.forChild([{
        path: "",
        component: UploadUserPictureComponent
      }])
    ],
    declarations: [UploadUserPictureComponent],
    exports: [UploadUserPictureComponent]
})
export class UploadUserPictureModule {
}
