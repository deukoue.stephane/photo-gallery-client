import {Component, OnDestroy, OnInit} from "@angular/core";

/**
 * @author Stephane Deukoue
 */
@Component({
    selector: "navbar-component",
    templateUrl: "./navbar.component.html",
    styleUrls: ["navbar.component.scss"]
})
export class NavbarComponent implements OnInit, OnDestroy {

    constructor() {}

    ngOnInit(): void {}

    ngOnDestroy(): void {}
}
