import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {NavbarComponent} from "./navbar.component";

/**
 * @author Stephane Deukoue
 */
@NgModule({
    declarations: [
        NavbarComponent,
    ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: "",
      component: NavbarComponent
    }])
  ],
    exports: [
        NavbarComponent,
    ]
})
export class NavbarModule {
}
