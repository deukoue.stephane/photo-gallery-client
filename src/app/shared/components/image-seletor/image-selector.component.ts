import {Component, EventEmitter, Inject, Input, OnInit, Output} from "@angular/core";
import {Picture} from "../../../dto/dto";

/**
 * @author Stephane Deukoue
 */
@Component({
    selector: "app-image-selector",
    templateUrl: "./image-selector.component.html",
    styleUrls: ["image-selector.component.scss"]
})
export class ImageSelectorComponent implements OnInit {
    /** Holds the list of pictures to display */
    @Input() pictures: Picture[];
  /** used when the user wants to edit the pictures he has uploaded */
    @Input() disabled: boolean = false;
    /** Used to show the delete icon shown on the pictures */
    @Input() showTrash: boolean = true;
    /** Used to remove/add border on the image selector container */
    @Input() removeBorder: boolean = false;

    /** Emits list of selected pictures/files */
    @Output() readonly value: EventEmitter<Picture[]> = new EventEmitter<Picture[]>();

    selectedPictures: Picture[] = [];
    selectedIndices: number[] = [];
    selectAll: boolean = false;
    isEditing: boolean = false;

    private static selectAllHelper(array: any[], item: any): void {
        if (!array.includes(item)) {
            array.push(item);
        }
    }

    constructor() {
    }

    ngOnInit(): void {}

    edit(): void {
      this.isEditing = true
      this.showTrash = true;
      this.disabled = false;
    }

    cancel() {
      this.showTrash = false;
      this.disabled = true;
      this.isEditing = false;
      this.selectAll = false;
      this.selectedPictures = [];
      this.selectedIndices = [];
    }

    saveChanges(): void {
      this.value.emit(this.selectedPictures);
      this.cancel();
    }

    /** Helper function used to remove or push an item from/into the list
     * @param array the array that is going to be used to perform this action
     * @param item the item to remove or push
     *
     * @return any[] the given array after it has been modified
     */
    private removeOrPushItem(array: any[], item: any): any[] {
        if (array.includes(item)) {
            array = array.filter(i => i !== item);
        } else {
            array.push(item);
        }

        return array;
    }

    /** Removes or pushes an item from/into the list
     * @param index the index of the selected file/picture
     * @param picture the selected file/picture
     */
    selectOrRemovePicture(index: number, picture: Picture): void {
        if (!this.disabled) {
            this.selectedIndices = this.removeOrPushItem(this.selectedIndices, index);
            this.selectedPictures = this.removeOrPushItem(this.selectedPictures, picture);
        }
    }

    /** Selects all pictures/files */
    selectAllPictures(): void {
        this.selectAll = true;
        this.pictures?.forEach((picture, index) => {
            ImageSelectorComponent.selectAllHelper(this.selectedPictures, picture);
            ImageSelectorComponent.selectAllHelper(this.selectedIndices, index);
        });
    }

    /** Deselects all pictures/files */
    deSelectAllPictures(): void {
        this.selectAll = false;
        this.selectedIndices = [];
        this.selectedPictures = [];
    }

    /** Removes a picture from the selected ones
     * @param picture the picture to remove
     */
    deletePicture(picture: Picture): void {
      this.selectedPictures.push(picture)
      this.value.emit(this.selectedPictures)
      this.selectedPictures = [];
    }
}
