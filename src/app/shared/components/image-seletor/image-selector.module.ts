import {NgModule} from "@angular/core";
import {NgbPaginationModule} from "@ng-bootstrap/ng-bootstrap";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ImageSelectorComponent} from "./image-selector.component";
import {RouterModule} from "@angular/router";

/**
 * @author Stephane Deukoue
 */
@NgModule({
	imports: [
		CommonModule,
		NgbPaginationModule,
		FormsModule,
		RouterModule
	],
    declarations: [
        ImageSelectorComponent
    ],
    exports: [
        ImageSelectorComponent
    ]
})
export class ImageSelectorModule {}
