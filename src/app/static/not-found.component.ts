import {Component, Inject, OnDestroy, OnInit} from "@angular/core";
import {TitleService} from "../core/title.service";

/**
 * @author Dominik Hardtke
 */
@Component({
    selector: "not-found-page",
    templateUrl: "./not-found.component.html"
})
export class NotFoundComponent implements OnInit, OnDestroy {
    constructor(private titleService: TitleService) {
        this.titleService.setTitle("NOT_FOUND");
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
    }
}
